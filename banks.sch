EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Proasic3 Mezannine Card "
Date "May 13 2020"
Rev ""
Comp "Stockholm University"
Comment1 "katherine.dunne@fysik.su.se"
Comment2 "K. Dunne"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gbtx:A3P250-FGG144I U?
U 6 1 5E6980C1
P 3250 7100
AR Path="/5E6980C1" Ref="U?"  Part="6" 
AR Path="/5E58F90F/5E6980C1" Ref="U2"  Part="6" 
F 0 "U2" H 2700 7850 50  0000 L CNN
F 1 "A3P250-FGG144I" H 2400 7750 50  0000 L CNN
F 2 "gbtx-testboard:A39250-FGG144I" H 4200 7250 50  0001 C CNN
F 3 "" H 4200 7250 50  0001 C CNN
	6    3250 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6100 1400 6700
Wire Wire Line
	2450 6700 1400 6700
$Comp
L Device:R_Small R?
U 1 1 5E6980D6
P 2300 6300
AR Path="/5E6980D6" Ref="R?"  Part="1" 
AR Path="/5E58F90F/5E6980D6" Ref="R31"  Part="1" 
F 0 "R31" H 2359 6346 50  0000 L CNN
F 1 "4k7" H 2359 6255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2300 6300 50  0001 C CNN
F 3 "~" H 2300 6300 50  0001 C CNN
	1    2300 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E6980DC
P 2050 6300
AR Path="/5E6980DC" Ref="R?"  Part="1" 
AR Path="/5E58F90F/5E6980DC" Ref="R30"  Part="1" 
F 0 "R30" H 2109 6346 50  0000 L CNN
F 1 "4k7" H 2109 6255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2050 6300 50  0001 C CNN
F 3 "~" H 2050 6300 50  0001 C CNN
	1    2050 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E6980E2
P 1800 6300
AR Path="/5E6980E2" Ref="R?"  Part="1" 
AR Path="/5E58F90F/5E6980E2" Ref="R28"  Part="1" 
F 0 "R28" H 1859 6346 50  0000 L CNN
F 1 "4k7" H 1859 6255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1800 6300 50  0001 C CNN
F 3 "~" H 1800 6300 50  0001 C CNN
	1    1800 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R?
U 1 1 5E6980E8
P 1550 6300
AR Path="/5E6980E8" Ref="R?"  Part="1" 
AR Path="/5E58F90F/5E6980E8" Ref="R27"  Part="1" 
F 0 "R27" H 1609 6346 50  0000 L CNN
F 1 "4k7" H 1609 6255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1550 6300 50  0001 C CNN
F 3 "~" H 1550 6300 50  0001 C CNN
	1    1550 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 6100 1550 6100
Wire Wire Line
	1550 6200 1550 6100
Connection ~ 1550 6100
Wire Wire Line
	1550 6100 1800 6100
Wire Wire Line
	1800 6200 1800 6100
Connection ~ 1800 6100
Wire Wire Line
	1800 6100 2050 6100
Wire Wire Line
	2050 6200 2050 6100
Connection ~ 2050 6100
Wire Wire Line
	2050 6100 2300 6100
Wire Wire Line
	2300 6200 2300 6100
Wire Wire Line
	2300 6400 2300 7100
Wire Wire Line
	2300 7100 2450 7100
Wire Wire Line
	2050 6400 2050 7000
Wire Wire Line
	2050 7000 2450 7000
Wire Wire Line
	1800 6400 1800 6900
Wire Wire Line
	1800 6900 2450 6900
Wire Wire Line
	1550 6400 1550 6800
Wire Wire Line
	1550 6800 2450 6800
Connection ~ 2300 7100
Wire Wire Line
	850  7100 2300 7100
Connection ~ 2050 7000
Wire Wire Line
	850  7000 2050 7000
Connection ~ 1800 6900
Wire Wire Line
	850  6900 1800 6900
Connection ~ 1550 6800
Wire Wire Line
	850  6800 1550 6800
Text Label 850  7100 0    50   ~ 0
PROASIC_TDI
Text Label 850  7000 0    50   ~ 0
PROASIC_TMS
Text Label 850  6900 0    50   ~ 0
PROASIC_TDO
Text Label 850  6800 0    50   ~ 0
PROASIC_TCK
Text Label 5100 6850 2    50   ~ 0
PROASIC_TCK
Text Label 5100 6750 2    50   ~ 0
PROASIC_TDO
Text Label 5100 6650 2    50   ~ 0
PROASIC_TMS
Text Label 5100 6450 2    50   ~ 0
PROASIC_TDI
Wire Wire Line
	5100 6450 4500 6450
Wire Wire Line
	5100 6650 4500 6650
Wire Wire Line
	5100 6750 4500 6750
Wire Wire Line
	5100 6850 4500 6850
$Comp
L gbtx:87832-5623 J?
U 1 1 5E9605CE
P 3950 6350
AR Path="/5E9605CE" Ref="J?"  Part="1" 
AR Path="/5E58F90F/5E9605CE" Ref="J2"  Part="1" 
F 0 "J2" H 3975 6565 50  0000 C CNN
F 1 "87832-5623" H 3975 6474 50  0000 C CNN
F 2 "gbtx-testboard:878325623" H 3950 6350 50  0001 C CNN
F 3 "" H 3950 6350 50  0001 C CNN
	1    3950 6350
	1    0    0    -1  
$EndComp
Connection ~ 1250 1000
Wire Wire Line
	1500 1000 1500 1400
Wire Wire Line
	1250 1000 1500 1000
Wire Wire Line
	1000 1000 1250 1000
Wire Wire Line
	1000 1200 1250 1200
$Comp
L Device:C_Small C?
U 1 1 5E65B593
P 1250 1100
AR Path="/5E65B593" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E65B593" Ref="C13"  Part="1" 
F 0 "C13" H 1342 1146 50  0000 L CNN
F 1 ".33u" H 1342 1055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1250 1100 50  0001 C CNN
F 3 "~" H 1250 1100 50  0001 C CNN
	1    1250 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E65B58D
P 1000 1100
AR Path="/5E65B58D" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E65B58D" Ref="C11"  Part="1" 
F 0 "C11" H 1092 1146 50  0000 L CNN
F 1 ".01u" H 1092 1055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1000 1100 50  0001 C CNN
F 3 "~" H 1000 1100 50  0001 C CNN
	1    1000 1100
	1    0    0    -1  
$EndComp
Connection ~ 1350 1700
Connection ~ 1350 1600
Wire Wire Line
	1350 1600 1350 1700
Wire Wire Line
	1350 1500 1500 1500
Connection ~ 1350 1500
Wire Wire Line
	1350 1600 1500 1600
Wire Wire Line
	1350 1500 1350 1600
Wire Wire Line
	1200 1500 1350 1500
Connection ~ 1200 1500
Wire Wire Line
	950  1500 1200 1500
Wire Wire Line
	950  1700 1200 1700
$Comp
L Device:C_Small C?
U 1 1 5E65B576
P 1200 1600
AR Path="/5E65B576" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E65B576" Ref="C12"  Part="1" 
F 0 "C12" H 1292 1646 50  0000 L CNN
F 1 "10n" H 1292 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1200 1600 50  0001 C CNN
F 3 "~" H 1200 1600 50  0001 C CNN
	1    1200 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E65B570
P 950 1600
AR Path="/5E65B570" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E65B570" Ref="C10"  Part="1" 
F 0 "C10" H 1042 1646 50  0000 L CNN
F 1 "1u" H 1042 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 950 1600 50  0001 C CNN
F 3 "~" H 950 1600 50  0001 C CNN
	1    950  1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 3100 3250 3200
Connection ~ 3250 3100
Wire Wire Line
	3100 3100 3250 3100
Wire Wire Line
	3250 3000 3250 3100
Connection ~ 3250 3000
Wire Wire Line
	3100 3000 3250 3000
Wire Wire Line
	3250 2900 3250 3000
Connection ~ 3250 2900
Wire Wire Line
	3100 2900 3250 2900
Wire Wire Line
	3250 2800 3250 2900
Connection ~ 3250 2800
Wire Wire Line
	3100 2800 3250 2800
Wire Wire Line
	3250 2700 3250 2800
Connection ~ 3250 2700
Wire Wire Line
	3100 2700 3250 2700
Wire Wire Line
	3250 2600 3250 2700
Connection ~ 3250 2600
Wire Wire Line
	3100 2600 3250 2600
Wire Wire Line
	3250 2500 3250 2600
Connection ~ 3250 2500
Wire Wire Line
	3100 2500 3250 2500
Wire Wire Line
	3250 2400 3250 2500
Connection ~ 3250 2400
Wire Wire Line
	3100 2400 3250 2400
Wire Wire Line
	3250 2300 3250 2400
Connection ~ 3250 2300
Wire Wire Line
	3100 2300 3250 2300
Wire Wire Line
	3250 2200 3250 2300
Connection ~ 3250 2200
Wire Wire Line
	3100 2200 3250 2200
Wire Wire Line
	3250 2100 3250 2200
Connection ~ 3250 2100
Wire Wire Line
	3100 2100 3250 2100
Wire Wire Line
	3250 2000 3250 2100
Connection ~ 3250 2000
Wire Wire Line
	3100 2000 3250 2000
Wire Wire Line
	3250 1900 3250 2000
Connection ~ 3250 1900
Wire Wire Line
	3100 1900 3250 1900
Wire Wire Line
	3250 1800 3250 1900
Connection ~ 3250 1800
Wire Wire Line
	3100 1800 3250 1800
Wire Wire Line
	3250 1700 3250 1800
Connection ~ 3250 1700
Wire Wire Line
	3100 1700 3250 1700
Wire Wire Line
	3250 1600 3250 1700
Connection ~ 3250 1600
Wire Wire Line
	3100 1600 3250 1600
Wire Wire Line
	3250 1500 3250 1600
Connection ~ 3250 1500
Wire Wire Line
	3100 1500 3250 1500
Wire Wire Line
	3250 1400 3250 1500
Wire Wire Line
	3100 1400 3250 1400
Wire Wire Line
	1350 1800 1350 1900
Connection ~ 1350 1800
Wire Wire Line
	1500 1800 1350 1800
Wire Wire Line
	1350 1900 1350 2000
Connection ~ 1350 1900
Wire Wire Line
	1500 1900 1350 1900
Wire Wire Line
	1350 2000 1350 2100
Connection ~ 1350 2000
Wire Wire Line
	1500 2000 1350 2000
Wire Wire Line
	1350 2100 1350 2200
Connection ~ 1350 2100
Wire Wire Line
	1500 2100 1350 2100
Wire Wire Line
	1350 2200 1350 2300
Connection ~ 1350 2200
Wire Wire Line
	1500 2200 1350 2200
Wire Wire Line
	1350 2300 1350 2400
Connection ~ 1350 2300
Wire Wire Line
	1500 2300 1350 2300
Wire Wire Line
	1350 1700 1500 1700
Wire Wire Line
	1350 2400 1500 2400
Wire Wire Line
	1350 1700 1350 1800
$Comp
L gbtx:A3P250-FGG144I U?
U 5 1 5E65B519
P 2300 1800
AR Path="/5E65B519" Ref="U?"  Part="5" 
AR Path="/5E58F90F/5E65B519" Ref="U2"  Part="5" 
F 0 "U2" H 2300 2565 50  0000 C CNN
F 1 "A3P250-FGG144I" H 2300 2474 50  0000 C CNN
F 2 "gbtx-testboard:A39250-FGG144I" H 3250 1950 50  0001 C CNN
F 3 "" H 3250 1950 50  0001 C CNN
	5    2300 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  1000 1000 1000
Connection ~ 1000 1000
Wire Wire Line
	850  1500 950  1500
Connection ~ 950  1500
Wire Wire Line
	1250 6100 1400 6100
Connection ~ 1400 6100
Wire Wire Line
	3450 6650 3650 6650
Wire Notes Line
	700  5600 700  7600
Text Notes 800  5850 0    129  ~ 0
ProASIC JTAG
Connection ~ 7950 1050
Wire Wire Line
	7900 1050 7950 1050
Connection ~ 7950 1250
Wire Wire Line
	7900 1250 7950 1250
NoConn ~ 10150 2400
NoConn ~ 10150 2100
NoConn ~ 10150 2000
NoConn ~ 6550 2450
NoConn ~ 6550 2350
NoConn ~ 6550 2250
NoConn ~ 6550 2150
NoConn ~ 6550 2050
NoConn ~ 6550 1950
NoConn ~ 6550 1850
NoConn ~ 6550 1750
NoConn ~ 6550 1650
NoConn ~ 6550 1550
NoConn ~ 6550 1450
NoConn ~ 6550 1350
NoConn ~ 6550 1250
NoConn ~ 4800 2550
NoConn ~ 4800 2450
NoConn ~ 4800 2350
NoConn ~ 4800 2250
NoConn ~ 4800 2150
NoConn ~ 4800 2050
NoConn ~ 4800 1950
NoConn ~ 4800 1850
NoConn ~ 4800 1750
NoConn ~ 4800 1650
NoConn ~ 4800 1550
Connection ~ 4650 1350
Wire Wire Line
	4650 1450 4800 1450
Wire Wire Line
	4650 1350 4650 1450
Wire Wire Line
	4650 1250 4800 1250
Connection ~ 4650 1250
Wire Wire Line
	4650 1350 4800 1350
Wire Wire Line
	4650 1250 4650 1350
Wire Wire Line
	4300 1250 4650 1250
Connection ~ 8200 1050
Wire Wire Line
	8200 1050 8300 1050
Wire Wire Line
	7950 1250 8200 1250
Wire Wire Line
	8300 1200 8300 1300
Connection ~ 8300 1200
Wire Wire Line
	8450 1200 8300 1200
Wire Wire Line
	8300 1300 8300 1400
Connection ~ 8300 1300
Wire Wire Line
	8450 1300 8300 1300
Wire Wire Line
	8300 1400 8450 1400
Wire Wire Line
	8300 1050 8300 1200
Wire Wire Line
	7950 1050 8200 1050
$Comp
L Device:C_Small C?
U 1 1 5E6361DD
P 8200 1150
AR Path="/5E6361DD" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E6361DD" Ref="C18"  Part="1" 
F 0 "C18" H 8292 1196 50  0000 L CNN
F 1 "10n" H 8292 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8200 1150 50  0001 C CNN
F 3 "~" H 8200 1150 50  0001 C CNN
	1    8200 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E6361D7
P 7950 1150
AR Path="/5E6361D7" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E6361D7" Ref="C16"  Part="1" 
F 0 "C16" H 8042 1196 50  0000 L CNN
F 1 "1u" H 8042 1105 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7950 1150 50  0001 C CNN
F 3 "~" H 7950 1150 50  0001 C CNN
	1    7950 1150
	1    0    0    -1  
$EndComp
$Comp
L gbtx:A3P250-FGG144I U?
U 2 1 5E63618E
P 9250 1600
AR Path="/5E63618E" Ref="U?"  Part="2" 
AR Path="/5E58F90F/5E63618E" Ref="U2"  Part="2" 
F 0 "U2" H 9300 2365 50  0000 C CNN
F 1 "A3P250-FGG144I" H 9300 2274 50  0000 C CNN
F 2 "gbtx-testboard:A39250-FGG144I" H 10200 1750 50  0001 C CNN
F 3 "" H 10200 1750 50  0001 C CNN
	2    9250 1600
	1    0    0    -1  
$EndComp
$Comp
L gbtx:A3P250-FGG144I U?
U 1 1 5E636188
P 5600 1600
AR Path="/5E636188" Ref="U?"  Part="1" 
AR Path="/5E58F90F/5E636188" Ref="U2"  Part="1" 
F 0 "U2" H 5675 2315 50  0000 C CNN
F 1 "A3P250-FGG144I" H 5675 2224 50  0000 C CNN
F 2 "gbtx-testboard:A39250-FGG144I" H 6550 1750 50  0001 C CNN
F 3 "" H 6550 1750 50  0001 C CNN
	1    5600 1600
	1    0    0    -1  
$EndComp
Text HLabel 8300 1500 0    50   Input ~ 0
GBTX_TMS_REM_P
Text HLabel 8300 1600 0    50   Input ~ 0
GBTX_TMS_REM_N
Text HLabel 8300 1700 0    50   Input ~ 0
GBTX_TDI_REM_P
Text HLabel 8300 1800 0    50   Input ~ 0
GBTX_TDI_REM_N
Text HLabel 8300 2100 0    50   Input ~ 0
GBTX_TCK_REM_P
Text HLabel 8300 2200 0    50   Input ~ 0
GBTX_TCK_REM_N
Text HLabel 8300 2300 0    50   Input ~ 0
GBTX_TMS_LOC_P
Text HLabel 8300 2400 0    50   Input ~ 0
GBTX_TMS_LOC_N
Text HLabel 8300 2500 0    50   Input ~ 0
GBTX_TDI_LOC_P
Text HLabel 8300 2600 0    50   Input ~ 0
GBTX_TDI_LOC_N
Wire Wire Line
	8300 1500 8450 1500
Wire Wire Line
	8300 1600 8450 1600
Wire Wire Line
	8300 1700 8450 1700
Wire Wire Line
	8300 1800 8450 1800
Wire Wire Line
	8300 2100 8450 2100
Wire Wire Line
	8300 2200 8450 2200
Wire Wire Line
	8300 2300 8450 2300
Wire Wire Line
	8300 2400 8450 2400
Wire Wire Line
	8300 2500 8450 2500
Wire Wire Line
	8300 2600 8450 2600
Text HLabel 8300 1900 0    50   Input ~ 0
GBTX_RST_REM_P
Text HLabel 8300 2000 0    50   Input ~ 0
GBTX_RST_REM_N
Wire Wire Line
	8300 1900 8450 1900
Wire Wire Line
	8300 2000 8450 2000
Text HLabel 10350 1200 2    50   Input ~ 0
GBTX_RST_LOC_P
Text HLabel 10350 1300 2    50   Input ~ 0
GBTX_RST_LOC_N
Text HLabel 10350 1400 2    50   Input ~ 0
GBTX_TCK_LOC_P
Text HLabel 10350 1500 2    50   Input ~ 0
GBTX_TCK_LOC_N
Text HLabel 10350 1600 2    50   Input ~ 0
GBTX_KEY_A_P
Text HLabel 10350 1700 2    50   Input ~ 0
GBTX_KEY_A_N
Text HLabel 10350 1900 2    50   Input ~ 0
GBTX_KEY_B_P
Text HLabel 10350 1800 2    50   Input ~ 0
GBTX_KEY_B_N
Wire Wire Line
	10350 1200 10150 1200
Wire Wire Line
	10350 1300 10150 1300
Wire Wire Line
	10350 1400 10150 1400
Wire Wire Line
	10350 1500 10150 1500
Wire Wire Line
	10350 1600 10150 1600
Wire Wire Line
	10350 1700 10150 1700
Wire Wire Line
	10350 1800 10150 1800
Wire Wire Line
	10350 1900 10150 1900
Text HLabel 10350 2200 2    50   Input ~ 0
GBTX_DCLK6_N
Text HLabel 10350 2300 2    50   Input ~ 0
GBTX_DCLK6_P
Text GLabel 900  1000 0    50   Input ~ 0
3V3
Text GLabel 850  1500 0    50   Input ~ 0
1V5
Text GLabel 7900 1050 0    50   Input ~ 0
1V8
Text GLabel 1250 6100 0    50   Input ~ 0
1V8
Text GLabel 900  1200 0    50   Input ~ 0
GND
Wire Wire Line
	900  1200 1000 1200
Connection ~ 1000 1200
Text GLabel 850  1700 0    50   Input ~ 0
GND
Wire Wire Line
	850  1700 950  1700
Connection ~ 950  1700
Text GLabel 3250 3200 2    50   Input ~ 0
GND
Text GLabel 4300 1250 0    50   Input ~ 0
GND
Text GLabel 7900 1250 0    50   Input ~ 0
GND
Text GLabel 3500 6450 0    50   Input ~ 0
GND
Text GLabel 3450 6650 0    50   Input ~ 0
1V8
Text Notes 700  750  0    129  ~ 0
ProAsic Banks
Text Label 1850 7200 0    50   ~ 0
PROASIC_TRST
Wire Wire Line
	3500 6450 3650 6450
Text Label 3050 6550 0    50   ~ 0
PROASIC_TRST
Text GLabel 4950 6550 2    50   Input ~ 0
3V3
Wire Wire Line
	4950 6550 4500 6550
Text GLabel 3500 6850 0    50   Input ~ 0
GND
Wire Wire Line
	3500 6850 3650 6850
Wire Notes Line
	5250 5600 5250 7600
Wire Notes Line
	700  7600 5250 7600
NoConn ~ 3650 6750
Wire Wire Line
	1850 7200 2450 7200
Wire Wire Line
	3050 6550 3650 6550
Wire Wire Line
	10350 2200 10150 2200
Wire Wire Line
	10150 2300 10350 2300
Text Notes 8900 5600 0    129  ~ 0
GBTX JTAG Reset
Text GLabel 9500 5850 0    50   Input ~ 0
GND
Wire Wire Line
	9500 5850 9700 5850
Wire Wire Line
	8900 5950 9700 5950
Wire Wire Line
	8900 5750 9700 5750
Text Label 8900 5950 0    50   ~ 0
FPGA_RST_LOC
Text Label 8900 5750 0    50   ~ 0
FPGA_RST_REM
$Comp
L proasic-mezzanine-card-rescue:Header3-gbtx U?
U 1 1 5F5F2A95
P 9850 5750
AR Path="/5F5F2A95" Ref="U?"  Part="1" 
AR Path="/5E58F90F/5F5F2A95" Ref="U4"  Part="1" 
F 0 "U4" H 10028 5701 50  0000 L CNN
F 1 "Header3" H 10028 5610 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 9850 5750 50  0001 C CNN
F 3 "" H 9850 5750 50  0001 C CNN
	1    9850 5750
	1    0    0    -1  
$EndComp
NoConn ~ 10200 4100
NoConn ~ 10200 4000
Text GLabel 7900 3650 0    50   Input ~ 0
GND
Text GLabel 4250 3650 0    50   Input ~ 0
GND
Text GLabel 7900 3450 0    50   Input ~ 0
1V8
Text GLabel 4150 4350 0    50   Input ~ 0
2V5
Text GLabel 4150 4150 0    50   Input ~ 0
1V8
Text Label 7300 4100 2    50   ~ 0
FPGA_RST_LOC
Wire Wire Line
	8150 4900 8500 4900
Text HLabel 8150 4900 0    50   Input ~ 0
CB_OSC
Wire Wire Line
	10250 3600 10200 3600
Wire Wire Line
	10200 3800 10250 3800
Wire Wire Line
	10200 3700 10250 3700
Text HLabel 10250 3800 2    50   Input ~ 0
TESTCLOCKOUT
Text HLabel 10250 3700 2    50   Input ~ 0
GBTX_RXDATAVALID
Text HLabel 10250 3600 2    50   Input ~ 0
GBTX_RXDY
$Comp
L proasic-mezzanine-card-rescue:Header3-gbtx U?
U 1 1 5E63627B
P 4450 4350
AR Path="/5E63627B" Ref="U?"  Part="1" 
AR Path="/5E58F90F/5E63627B" Ref="U3"  Part="1" 
F 0 "U3" H 4442 4575 50  0000 C CNN
F 1 "Header3" H 4442 4484 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4450 4350 50  0001 C CNN
F 3 "" H 4450 4350 50  0001 C CNN
	1    4450 4350
	1    0    0    1   
$EndComp
Text Label 4050 4250 0    50   ~ 0
VMV2
Wire Wire Line
	4050 4250 4300 4250
Wire Wire Line
	8350 3800 8500 3800
Wire Wire Line
	8200 3450 8350 3450
Connection ~ 8200 3450
Wire Wire Line
	7950 3450 8200 3450
Wire Wire Line
	7950 3650 8200 3650
Wire Wire Line
	8350 3600 8350 3700
Connection ~ 8350 3600
Wire Wire Line
	8500 3600 8350 3600
Wire Wire Line
	8350 3700 8350 3800
Connection ~ 8350 3700
Wire Wire Line
	8500 3700 8350 3700
Wire Wire Line
	8350 3450 8350 3600
$Comp
L Device:C_Small C?
U 1 1 5E636264
P 8200 3550
AR Path="/5E636264" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E636264" Ref="C19"  Part="1" 
F 0 "C19" H 8292 3596 50  0000 L CNN
F 1 "10n" H 8292 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8200 3550 50  0001 C CNN
F 3 "~" H 8200 3550 50  0001 C CNN
	1    8200 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E63625E
P 7950 3550
AR Path="/5E63625E" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E63625E" Ref="C17"  Part="1" 
F 0 "C17" H 8042 3596 50  0000 L CNN
F 1 "1u" H 8042 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7950 3550 50  0001 C CNN
F 3 "~" H 7950 3550 50  0001 C CNN
	1    7950 3550
	1    0    0    -1  
$EndComp
NoConn ~ 6550 4600
NoConn ~ 4850 5000
NoConn ~ 4850 4900
NoConn ~ 4850 4800
NoConn ~ 4850 4700
NoConn ~ 4850 4600
NoConn ~ 4850 4500
NoConn ~ 4850 4400
NoConn ~ 4850 4300
NoConn ~ 4850 4200
NoConn ~ 4850 4100
NoConn ~ 4850 4000
NoConn ~ 4850 3900
NoConn ~ 10200 4800
NoConn ~ 10200 4700
NoConn ~ 10200 4600
NoConn ~ 10200 4500
NoConn ~ 10200 4400
NoConn ~ 10200 4300
NoConn ~ 10200 4200
NoConn ~ 8500 5000
NoConn ~ 8500 4800
NoConn ~ 8500 4700
NoConn ~ 8500 4600
NoConn ~ 8500 4500
NoConn ~ 8500 4400
NoConn ~ 8500 4300
NoConn ~ 8500 4200
NoConn ~ 8500 4100
NoConn ~ 8500 4000
NoConn ~ 8500 3900
Wire Wire Line
	4700 3800 4850 3800
Wire Wire Line
	4550 3450 4700 3450
Connection ~ 4550 3450
Wire Wire Line
	4300 3450 4550 3450
Wire Wire Line
	4300 3650 4550 3650
Wire Wire Line
	4700 3600 4700 3700
Connection ~ 4700 3600
Wire Wire Line
	4850 3600 4700 3600
Wire Wire Line
	4700 3700 4700 3800
Connection ~ 4700 3700
Wire Wire Line
	4850 3700 4700 3700
Wire Wire Line
	4700 3450 4700 3600
$Comp
L Device:C_Small C?
U 1 1 5E6361F9
P 4550 3550
AR Path="/5E6361F9" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E6361F9" Ref="C15"  Part="1" 
F 0 "C15" H 4642 3596 50  0000 L CNN
F 1 "10n" H 4642 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4550 3550 50  0001 C CNN
F 3 "~" H 4550 3550 50  0001 C CNN
	1    4550 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5E6361F3
P 4300 3550
AR Path="/5E6361F3" Ref="C?"  Part="1" 
AR Path="/5E58F90F/5E6361F3" Ref="C14"  Part="1" 
F 0 "C14" H 4392 3596 50  0000 L CNN
F 1 "1u" H 4392 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4300 3550 50  0001 C CNN
F 3 "~" H 4300 3550 50  0001 C CNN
	1    4300 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 3700 7300 3700
Wire Wire Line
	6550 4100 7300 4100
Text Label 7300 3700 2    50   ~ 0
FPGA_RST_REM
$Comp
L gbtx:A3P250-FGG144I U?
U 3 1 5E636194
P 5650 4000
AR Path="/5E636194" Ref="U?"  Part="3" 
AR Path="/5E58F90F/5E636194" Ref="U2"  Part="3" 
F 0 "U2" H 5700 4765 50  0000 C CNN
F 1 "A3P250-FGG144I" H 5700 4674 50  0000 C CNN
F 2 "gbtx-testboard:A39250-FGG144I" H 6600 4150 50  0001 C CNN
F 3 "" H 6600 4150 50  0001 C CNN
	3    5650 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 3650 4300 3650
Connection ~ 4300 3650
Wire Wire Line
	7900 3650 7950 3650
Connection ~ 7950 3650
Wire Wire Line
	7900 3450 7950 3450
Connection ~ 7950 3450
Wire Wire Line
	4150 4350 4300 4350
Wire Wire Line
	4150 4150 4300 4150
NoConn ~ 10200 3900
$Comp
L gbtx:A3P250-FGG144I U?
U 4 1 5E63620C
P 9300 4000
AR Path="/5E63620C" Ref="U?"  Part="4" 
AR Path="/5E58F90F/5E63620C" Ref="U2"  Part="4" 
F 0 "U2" H 9350 4765 50  0000 C CNN
F 1 "A3P250-FGG144I" H 9350 4674 50  0000 C CNN
F 2 "gbtx-testboard:A39250-FGG144I" H 10250 4150 50  0001 C CNN
F 3 "" H 10250 4150 50  0001 C CNN
	4    9300 4000
	1    0    0    -1  
$EndComp
Text HLabel 6750 3600 2    50   Input ~ 0
FPGA_TMS_REM
Text HLabel 6750 3800 2    50   Input ~ 0
FPGA_TDI_REM
Text HLabel 6750 3900 2    50   Input ~ 0
FPGA_TCK_REM
Text HLabel 6750 4000 2    50   Input ~ 0
FPGA_TMS_LOC
Text HLabel 6750 4200 2    50   Input ~ 0
FPGA_TDI_LOC
Text HLabel 6750 4300 2    50   Input ~ 0
FPGA_TCK_LOC
Wire Wire Line
	6750 3600 6550 3600
Wire Wire Line
	6750 3800 6550 3800
Wire Wire Line
	6750 3900 6550 3900
Wire Wire Line
	6750 4000 6550 4000
Wire Wire Line
	6750 4200 6550 4200
Wire Wire Line
	6750 4300 6550 4300
Connection ~ 4300 3450
Wire Wire Line
	4050 3450 4300 3450
Text Label 4050 3450 0    50   ~ 0
VMV2
Text Label 7150 4400 2    50   ~ 0
FPGA_MON_1
Wire Wire Line
	7150 4400 6550 4400
Text Label 7150 4500 2    50   ~ 0
FPGA_MON_2
Wire Wire Line
	7150 4500 6550 4500
$Comp
L gbtx:Header3 U9
U 1 1 5EB49CC5
P 7400 5200
F 0 "U9" H 7578 5151 50  0000 L CNN
F 1 "Header3" H 7578 5060 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7400 5200 50  0001 C CNN
F 3 "" H 7400 5200 50  0001 C CNN
	1    7400 5200
	1    0    0    -1  
$EndComp
Text Label 6650 5200 0    50   ~ 0
FPGA_MON_1
Wire Wire Line
	6650 5200 7250 5200
Text Label 6650 5400 0    50   ~ 0
FPGA_MON_2
Wire Wire Line
	6650 5400 7250 5400
Text Label 6950 5300 0    50   ~ 0
GND
Wire Wire Line
	6950 5300 7250 5300
Wire Notes Line
	700  5600 5250 5600
$EndSCHEMATC
